import 'package:attendance_tracker/models/event/eventParticipant/event_participant.dart';
import 'package:attendance_tracker/services/db/event_participant_database.dart';

import '../../models/event/eventModel/event_model.dart';
import '../db/event_model_database.dart';

class EventParticipantService {
  final EventParticipantDatabase _database = EventParticipantDatabase();
  final EventModelDatabase _eventModelDatabase = EventModelDatabase();

  Future<bool> addParticipantToDb(EventParticipant eventParticipant) async {
    try {
      await _database.addParticipant(eventParticipant);
      return true;
    } catch (e) {
      throw Exception(
          'Error adding participant on the service layer due to $e');
    }
  }

  Future<bool> addParticipantToEvent(
      int eventId, EventParticipant eventParticipant) async {
    try {
      EventModel eventModel = await _eventModelDatabase.getEventById(eventId);
      final id = await _database.addParticipant(eventParticipant);
      List<EventParticipant> currentParticipants =
          eventModel.peoplePresent ?? [];
      eventParticipant.id = id;
      currentParticipants.add(eventParticipant);
      if (eventParticipant.isPaid) {
        eventModel.amountOfMoneyPaid += eventParticipant.moneyPaid;
      }
      await _eventModelDatabase.updateEvent(eventModel);
      return true;
    } catch (e) {
      throw Exception(
          'Failed to add participant to event on service layer due to $e');
    }
  }

  Future<bool> deleteParticipantFromEvent(
      int eventId, EventParticipant eventParticipant) async {
    try {
      EventModel eventModel = await _eventModelDatabase.getEventById(eventId);

      List<EventParticipant> updatedParticipants =
          eventModel.peoplePresent!.where((participant) {
        return participant.id != eventParticipant.id;
      }).toList();

      eventModel.peoplePresent = updatedParticipants;

      await _eventModelDatabase.updateEvent(eventModel);

      await _database.deleteParticipant(eventParticipant.id!);

      return true;
    } catch (e) {
      print(e);
      throw Exception('Failed to delete participant from event: $e');
    }
  }

  Future<bool> deleteEventParticipant(int participantId) async {
    try {
      await _database.deleteParticipant(participantId);
      return true;
    } catch (e) {
      print(e);
      throw Exception('Error deleting training Session');
    }
  }

  Future<bool> updateParticipant(EventParticipant eventParticipant) async {
    try {
      await _database.updateParticipant(eventParticipant);
      return true;
    } catch (e) {
      print(e);
      throw Exception('Error updating training Session');
    }
  }

  Future<bool> updateParticipantInEvent(
      int eventId, EventParticipant updatedParticipant) async {
    try {
      EventModel eventToUpdate =
          await _eventModelDatabase.getEventById(eventId);

      List<EventParticipant> updatedParticipants =
          eventToUpdate.peoplePresent!.map((participant) {
        if (participant.id == updatedParticipant.id) {
          return updatedParticipant;
        } else {
          return participant;
        }
      }).toList();

      eventToUpdate.peoplePresent = updatedParticipants;
      await _eventModelDatabase.updateEvent(eventToUpdate);
      return true;
    } catch (e) {
      throw Exception('Failed to update isPaid in the service layer due to $e');
    }
  }

  Future<List<EventParticipant>> getAllParticipants() async {
    try {
      return await _database.getAllParticipants();
    } catch (e) {
      // Handle any exceptions here
      throw Exception('Failed to get all training sessions: $e');
    }
  }
}
