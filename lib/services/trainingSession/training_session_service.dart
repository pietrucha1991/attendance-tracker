import 'package:attendance_tracker/models/trainingSession/trainingSession.dart';
import 'package:attendance_tracker/services/db/database_helper.dart';
import 'package:attendance_tracker/services/db/training_session_database.dart';

import '../../models/trainee/trainee.dart';

class TrainingSessionService {
  final TrainingSessionDatabase _trainingSessionDatabase =
      TrainingSessionDatabase();

  Future<bool> addTrainingSession(TrainingSession trainingSession) async {
    try {
      await _trainingSessionDatabase.addTrainingSession(trainingSession);
      return true;
    } catch (e) {
      print(e);
      throw Exception('Error adding training Session');
    }
  }

  Future<bool> addTraineeToTrainingSession(
      int sessionId, Trainee trainee) async {
    try {
      TrainingSession trainingSession =
          await _trainingSessionDatabase.getTrainingSessionById(sessionId);
      await _trainingSessionDatabase.addTraineeToTrainingSession(
          trainee, trainingSession);

      print('Successs in adding trainee to trainingsession');
      return true;
    } catch (e) {
      throw Exception('$e');
    }
  }

  Future<bool> deleteTrainingSession(int id) async {
    try {
      await _trainingSessionDatabase.deleteTrainingSession(id);
      return true;
    } catch (e) {
      print(e);
      throw Exception('Error deleting training Session');
    }
  }

  Future<bool> updateTrainingSession(TrainingSession trainingSession) async {
    try {
      await _trainingSessionDatabase.updateTrainingSession(trainingSession);
      return true;
    } catch (e) {
      print(e);
      throw Exception('Error updating training Session');
    }
  }

  Future<List<TrainingSession>> getAllTrainingSessions() async {
    try {
      return await _trainingSessionDatabase.getAllTrainingSessions();
    } catch (e) {
      // Handle any exceptions here
      throw Exception('Failed to get all training sessions: $e');
    }
  }

  Future<List<TrainingSession>> getAllTrainingSessionsForDay(
      DateTime dateTime) async {
    try {
      return await _trainingSessionDatabase
          .getAllTrainingSessionsForDay(dateTime);
    } catch (e) {
      // Handle any exceptions here
      throw Exception('Failed to get all training sessions: $e');
    }
  }

  Future<TrainingSession> getTrainingSessionById(int sessionId) async {
    try {
      return await _trainingSessionDatabase.getTrainingSessionById(sessionId);
    } catch (e) {
      // Handle any exceptions here
      throw Exception('Failed to get training session: $e');
    }
  }
}
