import 'package:shared_preferences/shared_preferences.dart';

class PreferencesService {
  Future<DateTime?> getLastResetDate() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String? lastResetDateString = prefs.getString('last_reset_date');
    if (lastResetDateString == null) {
      return null;
    }
    return DateTime.parse(lastResetDateString);
  }

  Future<void> setLastResetDate(DateTime date) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('last_reset_date', date.toIso8601String());
  }
}
