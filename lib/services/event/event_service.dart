import 'dart:async';

import 'package:attendance_tracker/services/db/event_model_database.dart';

import '../../models/event/eventModel/event_model.dart';

class EventService {
  final EventModelDatabase _database = EventModelDatabase();

  Future<bool> addEvent(String title, String description, DateTime startDate,
      DateTime endDate) async {
    try {
      EventModel eventToAdd = EventModel(
          title: title,
          description: description,
          startDate: startDate,
          endDate: endDate);

      final int id = await _database.addEvent(eventToAdd);
      return id > 0;
    } catch (e) {
      throw Exception('Failed to add event from Service layer because of $e');
    }
  }

  Future<List<EventModel>> getAllEvents() async {
    return await _database.getAllEvents();
  }

  Future<EventModel> getEventById(int eventId) async {
    return await _database.getEventById(eventId);
  }

  Future<bool> deleteEvent(int eventId) async {
    try {
      await _database.deleteEvent(eventId);
      return true;
    } catch (e) {
      throw Exception('Failed to delete event from a service layer due to $e');
    }
  }

  Future<bool> updateEvent(EventModel eventModel) async {
    try {
      await _database.updateEvent(eventModel);
      return true;
    } catch (e) {
      throw Exception('Failed to update event from a service layer due to $e');
    }
  }
}
