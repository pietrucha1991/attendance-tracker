import '../../models/payment/payment_model.dart';
import '../db/database_helper.dart';

class PaymentService {
  final DatabaseHelper _databaseHelper = DatabaseHelper.instance;

  Future<bool> addPayment(double moneyPaid, int traineeId, String description,
      DateTime dateOfPayment) async {
    try {
      Payment paymentToAdd = Payment(
          traineeId: traineeId,
          dateOfPayment: dateOfPayment,
          description: description,
          amountPaid: moneyPaid);
      final int id = await _databaseHelper.addPayment(paymentToAdd);
      print('Payment added ');
      return id > 0;
    } catch (e) {
      throw Exception('Unable to add payment due to $e');
    }
  }

  Future<List<Payment>> getAllPayments() async {
    return await _databaseHelper.getAllPayments();
  }

  Future<List<Payment>> getAllPaymentsForTrainee(int traineeId) async {
    return await _databaseHelper.getAllPaymentsForTrainee(traineeId);
  }

  Future<Payment?> getPaymentById(int paymentId) async {
    return await _databaseHelper.getPaymentById(paymentId);
  }

  Future<bool> updatePayment(Payment payment) async {
    try {
      await _databaseHelper.updatePayment(payment);
      print('Payment updated ');
      return true;
    } catch (e) {
      throw Exception('Unable to update payment due to $e');
    }
  }

  Future<bool> deletePayment(int paymentId) async {
    try {
      await _databaseHelper.deletePayment(paymentId);
      print('User deleted ');
      return true;
    } catch (e) {
      throw Exception('Unable to delete payment due to $e');
    }
  }
}
