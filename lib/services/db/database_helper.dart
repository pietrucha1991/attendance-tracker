import 'dart:convert';

import 'package:attendance_tracker/models/trainingSession/trainingSession.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

import '../../models/event/eventModel/event_model.dart';
import '../../models/payment/payment_model.dart';
import '../../models/trainee/trainee.dart';

class DatabaseHelper {
  static const int _version = 18;
  static const String _dbName = "Trainee.db";
  static const String traineesTableName = 'trainees';
  static const String trainingSessionTableName = 'trainingSessions';
  static const String paymentsTable = 'payments';
  static const String eventsTable = 'events';
  static const String eventParticipantTable = 'eventParticipant';
  static final DatabaseHelper instance = DatabaseHelper._init();
  static Database? _database;
  static const String idType = 'INTEGER PRIMARY KEY AUTOINCREMENT';
  static const String textType = 'TEXT NOT NULL';
  static const String doubleType = 'REAL NOT NULL';
  static const String boolType = 'INTEGER NOT NULL DEFAULT 0';

  DatabaseHelper._init();

  Future<Database> get database async {
    if (_database != null) {
      return _database!;
    } else {
      _database = await _initDb(_dbName);
      return _database!;
    }
  }

  Future _createDB(Database db, int version) async {
    await db.execute('''
    CREATE TABLE $traineesTableName (
        id $idType,
        firstName $textType, 
        lastName $textType, 
        trainingPrice $doubleType,
        isPaid $boolType)
    ''');
    await db.execute('''
    CREATE TABLE IF NOT EXISTS $trainingSessionTableName(
    id $idType,
    trainingType $textType,
    date $textType,
    traineesPresent TEXT
   ) ''');
    await db.execute('''
      CREATE TABLE IF NOT EXISTS $paymentsTable (
    id $idType,
    traineeId INTEGER NOT NULL,
    amountPaid REAL NOT NULL,
    dateOfPayment TEXT NOT NULL,
    description TEXT,
    FOREIGN KEY (traineeId) REFERENCES trainees(id)
     );
      ''');
    await db.execute('''
        CREATE TABLE IF NOT EXISTS $eventsTable(
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          title TEXT NOT NULL,
          description TEXT NOT NULL,
          peoplePresent TEXT NOT NULL,
          amountOfMoneyPaid REAL,
          expenses TEXT,
          startDate TEXT NOT NULL,
           endDate TEXT NOT NULL
        )
      ''');
    await db.execute('''
        CREATE TABLE IF NOT EXISTS $eventParticipantTable(
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          name TEXT NOT NULL,
          moneyPaid REAL,
          isPaid $boolType
        )
      ''');
  }

  Future _onUpgrade(Database db, int oldVersion, int newVersion) async {
    if (oldVersion < 2) {
      await db.execute(
          'ALTER TABLE trainees ADD COLUMN isPaid INTEGER NOT NULL DEFAULT 0');
    }
    if (oldVersion < 9) {
      await db
          .execute('ALTER TABLE $paymentsTable ADD COLUMN description TEXT');
    }
    if (newVersion > oldVersion) {
      await db.execute('''
    CREATE TABLE IF NOT EXISTS $trainingSessionTableName(
    id $idType,
    trainingType $textType,
    date $textType,
    traineesPresent TEXT
   ) ''');

      await db.execute('''
      CREATE TABLE IF NOT EXISTS $paymentsTable (
    id $idType,
    traineeId INTEGER NOT NULL,
    amountPaid REAL NOT NULL,
    dateOfPayment TEXT NOT NULL,
    description TEXT,
    FOREIGN KEY (traineeId) REFERENCES trainees(id)
     );
      ''');

      await db.execute('''
        CREATE TABLE IF NOT EXISTS  $eventsTable(
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          title TEXT NOT NULL,
          description TEXT NOT NULL,
          peoplePresent TEXT NOT NULL,
          amountOfMoneyPaid REAL,
          expenses TEXT,
          startDate TEXT NOT NULL,
          endDate TEXT NOT NULL 
        )
      ''');
      await db.execute('''
        CREATE TABLE IF NOT EXISTS  $eventParticipantTable(
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          name TEXT NOT NULL,
          moneyPaid REAL,
          isPaid $boolType
        )
      ''');
    }
  }

  Future<Database> _initDb(String pathFile) async {
    final dbPath = await getApplicationDocumentsDirectory();

    final path = join(dbPath.path, pathFile);
    print('PATH TO THE DB FILE  $path');
    return openDatabase(path,
        version: _version, onCreate: _createDB, onUpgrade: _onUpgrade);
  }

  Future close() async {
    final db = await instance.database;
    db.close();
  }

  //Trainee CRUD
  Future<Trainee?> getTraineeById(int id) async {
    final db = await instance.database;
    List<Map<String, dynamic>> traineesMap =
        await db.query(traineesTableName, where: 'id = ?', whereArgs: [id]);
    if (traineesMap.isNotEmpty) {
      return Trainee.fromJson(traineesMap.first);
    } else {
      throw Exception('User not found');
    }
  }

  Future<int> addTrainee(Trainee trainee) async {
    final db = await instance.database;
    final json = trainee.toJson();
    json.remove('id');
    return await db.insert(traineesTableName, json);
  }

  Future<void> deleteTrainee(int id) async {
    final db = await instance.database;

    await db.delete(traineesTableName, where: 'id = ?', whereArgs: [id]);
  }

  Future<void> updateTrainee(Trainee trainee) async {
    final db = await instance.database;
    final json = trainee.toJson();

    await db.update(traineesTableName, json,
        where: 'id = ?', whereArgs: [trainee.id]);
  }

  Future<List<Trainee>> getAllTrainees() async {
    final db = await instance.database;
    final result = await db.query(traineesTableName);

    return result.map((json) => Trainee.fromJson(json)).toList();
  }

  //Payment CRUD
  Future<Payment> getPaymentById(int paymentId) async {
    final db = await instance.database;
    List<Map<String, dynamic>> paymentMap =
        await db.query(paymentsTable, where: 'id = ?', whereArgs: [paymentId]);
    try {
      return Payment.fromJson(paymentMap.first);
    } catch (e) {
      throw Exception('Unable to fetch payment by id');
    }
  }

  Future<List<Payment>> getAllPayments() async {
    final db = await instance.database;
    final result = await db.query(paymentsTable);

    return result.map((json) => Payment.fromJson(json)).toList();
  }

  Future<List<Payment>> getAllPaymentsForTrainee(int traineeId) async {
    final db = await instance.database;
    final result = await db
        .query(paymentsTable, where: 'traineeId = ?', whereArgs: [traineeId]);

    return result.map((json) => Payment.fromJson(json)).toList();
  }

  Future<int> addPayment(Payment payment) async {
    final db = await instance.database;
    final json = payment.toJson();
    json.remove('id');
    return await db.insert(paymentsTable, json);
  }

  Future<void> deletePayment(int paymentId) async {
    final db = await instance.database;

    await db.delete(paymentsTable, where: 'id = ?', whereArgs: [paymentId]);
  }

  Future<void> updatePayment(Payment payment) async {
    final db = await instance.database;
    final json = payment.toJson();

    await db
        .update(paymentsTable, json, where: 'id = ?', whereArgs: [payment.id]);
  }

  //Event Crud
}
