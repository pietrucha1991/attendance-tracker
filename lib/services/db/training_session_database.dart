import 'dart:convert';

import 'package:attendance_tracker/services/db/database_helper.dart';

import '../../models/trainee/trainee.dart';
import '../../models/trainingSession/trainingSession.dart';

class TrainingSessionDatabase {
  static const String trainingSessionTableName = 'trainingSessions';

  Future<int> addTrainingSession(TrainingSession trainingSession) async {
    final db = await DatabaseHelper.instance.database;
    final json = trainingSession.toJson();
    return await db.insert(trainingSessionTableName, json);
  }

  Future<void> deleteTrainingSession(int id) async {
    final db = await DatabaseHelper.instance.database;
    await db.delete(trainingSessionTableName, where: 'id = ?', whereArgs: [id]);
  }

  Future<void> updateTrainingSession(TrainingSession trainingSession) async {
    final db = await DatabaseHelper.instance.database;
    final json = trainingSession.toJson();

    await db.update(trainingSessionTableName, json,
        where: 'id = ?', whereArgs: [trainingSession.id]);
  }

  Future<void> addTraineeToTrainingSession(
      Trainee trainee, TrainingSession trainingSession) async {
    final db = await DatabaseHelper.instance.database;
    List<Trainee> currentTrainees = trainingSession.traineesPresent ?? [];
    currentTrainees.add(trainee);
    final traineesJson =
        currentTrainees.map((trainee) => trainee.toJson()).toList();

    await db.update(
        trainingSessionTableName, {'traineesPresent': jsonEncode(traineesJson)},
        where: 'id = ?', whereArgs: [trainingSession.id]);
  }

  Future<TrainingSession> getTrainingSessionById(int sessionId) async {
    final db = await DatabaseHelper.instance.database;
    List<Map<String, dynamic>> trainingSessionMap = await db.query(
        trainingSessionTableName,
        where: 'id = ?',
        whereArgs: [sessionId]);

    if (trainingSessionMap.isNotEmpty) {
      return TrainingSession.fromJson(trainingSessionMap.first);
    } else {
      throw Exception('Training session not found');
    }
  }

  Future<List<TrainingSession>> getAllTrainingSessions() async {
    final db = await DatabaseHelper.instance.database;
    final result = await db.query(trainingSessionTableName);

    return result.map((json) => TrainingSession.fromJson(json)).toList();
  }

  Future<List<TrainingSession>> getAllTrainingSessionsForDay(
      DateTime date) async {
    final db = await DatabaseHelper.instance.database;
    String dateString =
        "${date.year}-${date.month.toString().padLeft(2, '0')}-${date.day.toString().padLeft(2, '0')}";
    final result = await db.query(trainingSessionTableName,
        where: "DATE(date) = ?", whereArgs: [dateString]);

    return result.map((json) => TrainingSession.fromJson(json)).toList();
  }
}
