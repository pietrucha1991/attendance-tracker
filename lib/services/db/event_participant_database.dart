import 'package:attendance_tracker/models/event/eventParticipant/event_participant.dart';
import 'package:attendance_tracker/services/db/database_helper.dart';

class EventParticipantDatabase {
  static const String eventParticipantTable = 'eventParticipant';

  Future<List<EventParticipant>> getAllParticipants() async {
    final db = await DatabaseHelper.instance.database;
    final result = await db.query(eventParticipantTable);
    return result.map((json) => EventParticipant.fromJson(json)).toList();
  }

  Future<int> addParticipant(EventParticipant eventParticipant) async {
    final db = await DatabaseHelper.instance.database;
    return await db.insert(eventParticipantTable, eventParticipant.toJson());
  }

  Future<void> deleteParticipant(int participantId) async {
    final db = await DatabaseHelper.instance.database;
    await db.delete(eventParticipantTable,
        where: 'id = ?', whereArgs: [participantId]);
  }

  Future<void> updateParticipant(EventParticipant eventParticipant) async {
    final db = await DatabaseHelper.instance.database;
    final json = eventParticipant.toJson();
    await db.update(eventParticipantTable, json,
        where: 'id=?', whereArgs: [eventParticipant.id]);
  }
}
