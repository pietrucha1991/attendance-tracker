import 'dart:convert';

import 'package:attendance_tracker/models/event/eventParticipant/event_participant.dart';

import '../../models/event/eventModel/event_model.dart';
import 'database_helper.dart';

class EventModelDatabase {
  static const String eventsTable = 'events';

  Future<List<EventModel>> getAllEvents() async {
    final db = await DatabaseHelper.instance.database;
    final result = await db.query(eventsTable);

    return result.map((json) => EventModel.fromJson(json)).toList();
  }

  Future<EventModel> getEventById(int eventId) async {
    final db = await DatabaseHelper.instance.database;
    final result =
        await db.query(eventsTable, where: "id = ?", whereArgs: [eventId]);
    try {
      return EventModel.fromJson(result.first);
    } catch (e) {
      throw Exception('Failed to fetch event by id due to $e');
    }
  }

  Future<int> addEvent(EventModel eventModel) async {
    final db = await DatabaseHelper.instance.database;
    final json = eventModel.toJson();
    json.remove('id');
    return await db.insert(eventsTable, json);
  }

  Future<void> addParticipantToEvent(
      EventModel eventModel, EventParticipant eventParticipant) async {
    final db = await DatabaseHelper.instance.database;
    final participantsJson = eventModel.peoplePresent
        ?.map((participant) => participant.toJson())
        .toList();

    await db.update(
        eventsTable, {'peoplePresent': jsonEncode(participantsJson)},
        where: 'id = ?', whereArgs: [eventModel.id]);
  }

  Future<void> deleteEvent(int eventId) async {
    final db = await DatabaseHelper.instance.database;
    await db.delete(eventsTable, where: 'id = ?', whereArgs: [eventId]);
  }

  Future<void> updateEvent(EventModel event) async {
    final db = await DatabaseHelper.instance.database;
    await db.update(eventsTable, event.toJson(),
        where: 'id = ?', whereArgs: [event.id]);
  }
}
