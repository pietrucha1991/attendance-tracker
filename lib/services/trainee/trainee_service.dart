import 'package:attendance_tracker/models/trainee/trainee.dart';
import 'package:attendance_tracker/services/db/database_helper.dart';

class TraineeService {
  final DatabaseHelper _databaseHelper = DatabaseHelper.instance;

  Future<bool> addTrainee(
      String firstName, String lastName, double price) async {
    try {
      Trainee trainee = Trainee(
          firstName: firstName, lastName: lastName, trainingPrice: price);
      final int id = await _databaseHelper.addTrainee(trainee);
      return id > 0;
    } catch (e) {
      print('Error adding trainee $e');
      return false;
    }
  }

  Future<List<Trainee>> getAllTrainees() async {
    return await _databaseHelper.getAllTrainees();
  }

  Future<bool> deleteTrainee(int traineeId) async {
    try {
      await _databaseHelper.deleteTrainee(traineeId);
      print('User deleted successfully');
      return true;
    } catch (e) {
      print('$e');
      return false;
    }
  }

  Future<Trainee?> getTraineeById(int traineeId) async {
    return await _databaseHelper.getTraineeById(traineeId);
  }

  Future<bool> updateTrainee(Trainee trainee) async {
    try {
      await _databaseHelper.updateTrainee(trainee);
      return true;
    } catch (e) {
      print('Error updating trainee: $e');
      return false;
    }
  }
}
