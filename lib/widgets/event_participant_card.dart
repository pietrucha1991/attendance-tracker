import 'package:attendance_tracker/models/event/eventModel/event_model.dart';
import 'package:attendance_tracker/models/event/eventParticipant/event_participant.dart';
import 'package:attendance_tracker/services/event/event_service.dart';
import 'package:attendance_tracker/services/eventParticipant/event_participant_service.dart';
import 'package:attendance_tracker/widgets/green_gradient.dart';
import 'package:attendance_tracker/widgets/red_gradient.dart';
import 'package:flutter/material.dart';

class EventParticipantCard extends StatefulWidget {
  final EventParticipant eventParticipant;
  final EventModel eventModel;
  final VoidCallback onDeleted;

  //TODo:
  // 1. Update the cards here so that isPaid would be reflected in the DB.
  //
  EventParticipantCard(
      {Key? key,
      required this.eventModel,
      required this.eventParticipant,
      required this.onDeleted})
      : super(key: key);

  @override
  State<EventParticipantCard> createState() => _EventParticipantCardState();
}

class _EventParticipantCardState extends State<EventParticipantCard> {
  final EventParticipantService eventParticipantService =
      EventParticipantService();

  final EventService eventService = EventService();

  @override
  Widget build(BuildContext context) {
    return Card(
      child: widget.eventParticipant.isPaid
          ? GreenGradient(child: content())
          : RedGradient(child: content()),
    );
  }

  Widget content() {
    final eventParticipant = widget.eventParticipant;
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(
              '${widget.eventParticipant.name} ${widget.eventParticipant.moneyPaid}'),
          const Spacer(),
          Checkbox(
              value: eventParticipant.isPaid,
              onChanged: (bool? value) async {
                eventParticipant.isPaid = value ?? false;

                setState(() {
                  eventParticipant.isPaid == value;
                });
                if (eventParticipant.isPaid) {
                  widget.eventModel.amountOfMoneyPaid =
                      eventParticipant.moneyPaid +
                          widget.eventModel.amountOfMoneyPaid;
                  await eventService.updateEvent(widget.eventModel);
                }
                await eventParticipantService.updateParticipantInEvent(
                    widget.eventModel.id!, eventParticipant);
              }),
          IconButton(
              icon: const Icon(Icons.delete),
              onPressed: () async {
                await eventParticipantService.deleteParticipantFromEvent(
                    widget.eventModel.id!, widget.eventParticipant);
                widget.onDeleted();
              })
        ],
      ),
    );
  }
}
