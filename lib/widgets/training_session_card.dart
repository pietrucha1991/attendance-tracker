import 'package:attendance_tracker/widgets/green_gradient.dart';
import 'package:attendance_tracker/widgets/red_gradient.dart';
import 'package:flutter/material.dart';

import '../models/trainee/trainee.dart';

class TraineeCard extends StatelessWidget {
  final Trainee trainee;

  const TraineeCard({Key? key, required this.trainee}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: trainee.isPaid ? GreenGradient(child: content()) : RedGradient(child: content()),
    );
  }

  Widget content() {
    return Padding(padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text('${trainee.firstName} ${trainee.lastName}')
        ],
      ),
    );
  }
}
