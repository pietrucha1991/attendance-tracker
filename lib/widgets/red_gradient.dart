import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RedGradient extends StatelessWidget {
  final Widget child;

  const RedGradient({super.key, required this.child});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          gradient: LinearGradient(
              begin: Alignment.bottomRight,
              end: Alignment.topLeft,
              colors: [Colors.white38, Colors.red[900] ?? Colors.red]
          )
      ),
      child: child,
    );
  }
}