import 'package:attendance_tracker/services/trainee/trainee_service.dart';
import 'package:attendance_tracker/services/trainingSession/training_session_service.dart';
import 'package:attendance_tracker/widgets/green_gradient.dart';
import 'package:attendance_tracker/widgets/red_gradient.dart';
import 'package:attendance_tracker/widgets/training_session_card.dart';
import 'package:flutter/material.dart';

import '../models/trainee/trainee.dart';
import '../models/trainingSession/trainingSession.dart';

class TrainingSessionScreen extends StatefulWidget {
  final TrainingSession session;

  const TrainingSessionScreen({required this.session});

  @override
  State<TrainingSessionScreen> createState() => _TrainingSessionScreenState();
}

class _TrainingSessionScreenState extends State<TrainingSessionScreen>
    with WidgetsBindingObserver {
  List<Trainee> _trainees = [];

  Future<void> _loadTraineesForTheSession() async {
    TrainingSession session = await TrainingSessionService()
        .getTrainingSessionById(widget.session.id!);
    List<Trainee>? trainees = session.traineesPresent;
    List<Trainee> updatedTrainees = [];
    if (trainees != null) {
      for (var trainee in trainees) {
        Trainee? updatedTrainee =
            await TraineeService().getTraineeById(trainee.id!);
        updatedTrainees.add(updatedTrainee!);
      }
    }
    setState(() {
      _trainees = updatedTrainees;
    });
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    _loadTraineesForTheSession();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      _loadTraineesForTheSession();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Training session ${widget.session.date?.day}'),
      ),
      body: Column(children: <Widget>[
        Expanded(
          child: ListView.builder(
              itemCount: _trainees.length,
              itemBuilder: (context, index) {
                final trainee = _trainees[index];
                return TraineeCard(trainee: trainee);
              }),
        )
      ]),
    );
  }

  Card paidCard(Trainee trainee) {
    return Card(
      child: GreenGradient(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text('${trainee.firstName} ${trainee.lastName}'),
            ],
          ),
        ),
      ),
    );
  }

  Card notPaidCard(Trainee trainee) {
    return Card(
      child: RedGradient(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text('${trainee.firstName} ${trainee.lastName}'),
            ],
          ),
        ),
      ),
    );
  }
}
