import 'package:attendance_tracker/screens/event_screen.dart';
import 'package:attendance_tracker/screens/trainees_screen.dart';
import 'package:flutter/material.dart';

import 'calendar_screen.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Home Screen')),
      body: Center(
        child: Column(
          children: <Widget>[
            btnWidget(widget: TraineesScreen(), name: "Trainee"),
            const SizedBox(height: 20),
            btnWidget(widget: CalendarScreen(), name: "Calendar"),
            const SizedBox(height: 20),
            btnWidget(widget: EventScreen(), name: "Events")
          ],
        ),
      ),
    );
  }
}

class btnWidget extends StatelessWidget {
  final Widget widget;
  final String name;

  const btnWidget({super.key, required this.widget, required this.name});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () {
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => widget));
      },
      child: Ink(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [Colors.green[800]!, Colors.green[400]!],
                begin: Alignment.topLeft,
                end: Alignment.topRight,
              ),
              borderRadius: BorderRadius.circular(10.0)),
          child: Container(
            alignment: Alignment.center,
            constraints: BoxConstraints(minWidth: 200, minHeight: 50),
            child: Text('$name Screen'),
          )),
    );
  }
}
