import 'package:attendance_tracker/services/payment/payment_service.dart';
import 'package:attendance_tracker/widgets/green_gradient.dart';
import 'package:flutter/material.dart';

import '../models/payment/payment_model.dart';
import '../models/trainee/trainee.dart';

class PaymentScreen extends StatefulWidget {
  final Trainee trainee;

  PaymentScreen({required this.trainee});

  @override
  State<PaymentScreen> createState() => _PaymentScreenState();
}

class _PaymentScreenState extends State<PaymentScreen> {
  List<Payment> _allPaymentsForTrainee = [];
  final TextEditingController _priceController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();

  Future<void> _loadPaymentsForGivenTrainee() async {
    List<Payment> getAllPaymentsForTrainee =
        await PaymentService().getAllPaymentsForTrainee(widget.trainee.id!);
    setState(() {
      _allPaymentsForTrainee = getAllPaymentsForTrainee;
    });
  }

  @override
  void initState() {
    super.initState();
    _loadPaymentsForGivenTrainee();
  }

  void _showUpdatePaymentDialog(BuildContext context, Payment payment) {
    _priceController.text = payment.amountPaid.toString();
    _descriptionController.text = payment.description!;

    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Update payment'),
            content: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Flexible(
                    child: TextField(
                      controller: _descriptionController,
                      decoration: InputDecoration(labelText: 'Description'),
                    ),
                  ),
                  Flexible(
                    child: TextField(
                      controller: _priceController,
                      decoration: InputDecoration(labelText: 'Price'),
                    ),
                  ),
                ],
              ),
            ),
            actions: [
              TextButton(
                  onPressed: () async {
                    final paymentToUpdate = Payment(
                        id: payment.id,
                        description: _descriptionController.text,
                        amountPaid: double.parse(_priceController.text),
                        traineeId: payment.traineeId,
                        dateOfPayment: payment.dateOfPayment);
                    bool success =
                        await PaymentService().updatePayment(paymentToUpdate);
                    if (success) {
                      Navigator.of(context).pop();
                      setState(() {
                        _loadPaymentsForGivenTrainee();
                      });
                    } else {
                      throw Exception('Payment not updated');
                    }
                  },
                  child: Text('Update payment')),
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text('Cancel'))
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
            'Payments of ${widget.trainee.firstName} ${widget.trainee.lastName}'),
      ),
      body: Column(children: <Widget>[
        Expanded(
          child: ListView.builder(
              itemCount: _allPaymentsForTrainee.length,
              itemBuilder: (context, index) {
                final payment = _allPaymentsForTrainee[index];
                return GreenGradient(
                  child: ListTile(
                      onTap: () {
                        _showUpdatePaymentDialog(context, payment);
                      },
                      leading: const Icon(Icons.payments),
                      title: RichText(
                        text: TextSpan(children: [
                          const TextSpan(
                              text: 'Paid', style: TextStyle(fontSize: 16)),
                          TextSpan(
                              text: ' \$${payment.amountPaid}',
                              style: TextStyle(fontSize: 16)),
                          const TextSpan(
                            text: ' for ',
                            style: TextStyle(fontSize: 16),
                          ),
                          TextSpan(
                            text: '${payment.description}',
                            style: const TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold),
                          ),
                          const TextSpan(
                            text: ' on ',
                            style: TextStyle(fontSize: 16),
                          ),
                          TextSpan(
                            text:
                                '${payment.dateOfPayment.day}-${payment.dateOfPayment.month}-${payment.dateOfPayment.year}',
                            style: const TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold),
                          ),
                        ]),
                      ),
                      trailing: IconButton(
                        icon: const Icon(Icons.delete),
                        onPressed: () async {
                          await PaymentService().deletePayment(payment.id!);
                          _loadPaymentsForGivenTrainee();
                        },
                      )),
                );
              }),
        )
      ]),
    );
  }
}
