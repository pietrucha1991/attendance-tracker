import 'package:attendance_tracker/models/trainee/trainee.dart';
import 'package:attendance_tracker/models/trainingSession/trainingSession.dart';
import 'package:attendance_tracker/models/training_type.dart';
import 'package:attendance_tracker/screens/training_session_screen.dart';
import 'package:attendance_tracker/services/trainee/trainee_service.dart';
import 'package:attendance_tracker/services/trainingSession/training_session_service.dart';
import 'package:attendance_tracker/widgets/green_gradient.dart';
import 'package:flutter/material.dart';
import 'package:table_calendar/table_calendar.dart';

class CalendarScreen extends StatefulWidget {
  const CalendarScreen({super.key});

  @override
  State<CalendarScreen> createState() => _CalendarScreenState();
}

class _CalendarScreenState extends State<CalendarScreen> {
  late TrainingSessionService _trainingSessionUtility =
      TrainingSessionService();
  List<TrainingSession> _trainingSessions = [];
  Trainee? _selectedTrainee;
  List<Trainee> _trainees = [];
  TrainingType? _selectedType;
  DateTime _selectedDate = DateTime.now();
  DateTime _focusedDay = DateTime.now();

  Future<void> _getAllTrainees() async {
    _trainees = await TraineeService().getAllTrainees();
  }

  Future<void> _loadAllTrainingSessionsForDay(DateTime dateTime) async {
    List<TrainingSession> trainingSessions =
        await _trainingSessionUtility.getAllTrainingSessionsForDay(dateTime);
    setState(() {
      _trainingSessions = trainingSessions;
    });
  }

  @override
  void initState() {
    super.initState();
    _loadAllTrainingSessionsForDay(_selectedDate);
    _getAllTrainees();
  }

  void _onDaySelected(DateTime selectedDay, DateTime focusedDay) {
    setState(() {
      _selectedDate = selectedDay;
      _focusedDay = focusedDay;
    }); //yyy
    _loadAllTrainingSessionsForDay(selectedDay);
  }

  Future<void> reloadSessionTrainees(int sessionId) async {
    var sessionIndex =
        _trainingSessions.indexWhere((session) => session.id == sessionId);
    if (sessionIndex != -1) {
      TrainingSession updatedSession =
          await _trainingSessionUtility.getTrainingSessionById(sessionId);
      setState(() {
        _trainingSessions[sessionIndex] = updatedSession;
      });
    }
  }

  void _showAddTrainingSessionDialog(BuildContext context) {
    showDialog(
        context: context,
        builder: (context) {
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter setStateDialog) {
            return AlertDialog(
              alignment: Alignment.center,
              title: Text('Add training session'),
              actions: <Widget>[
                DropdownButton(
                  value: _selectedType,
                  isExpanded: true,
                  items: TrainingType.values.map((TrainingType type) {
                    return DropdownMenuItem<TrainingType>(
                      value: type,
                      child: Text(type.toString().split('.').last),
                    );
                  }).toList(),
                  onChanged: (TrainingType? newValue) {
                    setStateDialog(() {
                      _selectedType = newValue;
                    });
                  },
                ),
                TextField(
                  controller: TextEditingController(
                      text: _selectedDate.toString().split(' ')[0]),
                  readOnly: true,
                ),
                TextButton(
                    onPressed: () => _addTrainingSession(context),
                    child: Text("Add training session")),
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text('Cancel')),
              ],
            );
          });
        });
  }

  void _showAddMultipleTraineeToSessionDialog(
      BuildContext context, TrainingSession trainingSession) {
    List<Trainee>? traineesInTrainingSession = trainingSession.traineesPresent;
    List<Trainee> dialogTrainees = _trainees
        .where((trainee) => !traineesInTrainingSession!.contains(trainee))
        .toList();
    List<Trainee> newlyAddedTrainees = [];
    showDialog(
        context: context,
        builder: (context) {
          return StatefulBuilder(
            builder: (BuildContext context, StateSetter setStateDialog) {
              return AlertDialog(
                title: Text('Add trainees'),
                content: Container(
                  width: double.maxFinite,
                  child: ListView(
                    children: dialogTrainees.map((Trainee trainee) {
                      return CheckboxListTile(
                          title:
                              Text('${trainee.firstName} ${trainee.lastName}'),
                          value: traineesInTrainingSession?.contains(trainee),
                          onChanged: (bool? value) {
                            setStateDialog(() {
                              if (value == true) {
                                if (!traineesInTrainingSession!
                                    .contains(trainee)) {
                                  traineesInTrainingSession.add(trainee);
                                  newlyAddedTrainees.add(trainee);
                                }
                              } else {
                                traineesInTrainingSession?.remove(trainee);
                                dialogTrainees.remove(trainee);
                                newlyAddedTrainees.remove(trainee);
                              }
                            });
                          });
                    }).toList(),
                  ),
                ),
                actions: <Widget>[
                  TextButton(
                      onPressed: () async {
                        for (var trainee in newlyAddedTrainees) {
                          // assuming you want to process this updated list
                          await TrainingSessionService()
                              .addTraineeToTrainingSession(
                                  trainingSession.id!, trainee);
                        }
                        reloadSessionTrainees(trainingSession.id!);
                        Navigator.of(context).pop();
                      },
                      child: const Text('Add trainees')),
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text('Cancel'))
                ],
              );
            },
          );
        });
  }

  void _showAddTraineeToSessionDialog(
      BuildContext context, TrainingSession trainingSession) {
    showDialog(
        context: context,
        builder: (context) {
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter setStateDialog) {
            return AlertDialog(
              alignment: Alignment.center,
              title: Text('Add trainee'),
              actions: <Widget>[
                DropdownButton(
                    isExpanded: true,
                    value: _selectedTrainee,
                    items: _trainees.map((Trainee trainee) {
                      return DropdownMenuItem(
                          value: trainee,
                          child:
                              Text('${trainee.firstName} ${trainee.lastName}'));
                    }).toList(),
                    onChanged: (Trainee? trainee) {
                      setStateDialog(() {
                        _selectedTrainee = trainee;
                      });
                    }),
                TextButton(
                    onPressed: () async {
                      await TrainingSessionService()
                          .addTraineeToTrainingSession(
                              trainingSession.id!, _selectedTrainee!);
                      reloadSessionTrainees(trainingSession.id!);
                      Navigator.of(context).pop();
                    },
                    child: const Text('Add trainee')),
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: const Text('Cancel')),
              ],
            );
          });
        });
  }

  void _addTrainingSession(BuildContext context) async {
    bool isAdded = false;
    if (_selectedType != null) {
      TrainingSession sessionToAdd =
          TrainingSession(trainingType: _selectedType!, date: _selectedDate);
      isAdded = await TrainingSessionService().addTrainingSession(sessionToAdd);
    }

    if (isAdded) {
      print("Success in adding training session");
      Navigator.of(context).pop();
      _loadAllTrainingSessionsForDay(_selectedDate);
    } else {
      print("Success in adding training session");
    }
  }

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);

    return Scaffold(
      appBar: AppBar(title: Text('Calendar')),
      body: Center(
        child: Column(
          children: <Widget>[
            TableCalendar(
              calendarStyle: CalendarStyle(
                  selectedDecoration: BoxDecoration(
                color: theme.primaryColor,
                shape: BoxShape.circle,
              )),
              focusedDay: _focusedDay,
              firstDay: DateTime.utc(2000, 1, 1),
              lastDay: DateTime.utc(2030, 1, 1),
              selectedDayPredicate: (day) {
                return isSameDay(_selectedDate, day);
              },
              onDaySelected: _onDaySelected,
            ),
            Expanded(
                child: ListView.builder(
                    padding: const EdgeInsets.only(
                        bottom: kFloatingActionButtonMargin + 100.0),
                    itemCount: _trainingSessions.length,
                    itemBuilder: (context, index) {
                      final trainingSession = _trainingSessions[index];
                      int? traineesCount =
                          trainingSession.traineesPresent?.length;
                      return GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => TrainingSessionScreen(
                                      session: trainingSession)));
                        },
                        child: Card(
                            child: GreenGradient(
                          child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                            'Training group: ${trainingSession.trainingType.name}'),
                                        Text(
                                            'Date: ${trainingSession.date?.day}-${trainingSession.date?.month}-${trainingSession.date?.year}'),
                                        Text(
                                            '\nPresent trainees: $traineesCount'),
                                      ],
                                    ),
                                  ),
                                  Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      IconButton(
                                        onPressed: () =>
                                            _showAddTraineeToSessionDialog(
                                                context, trainingSession),
                                        icon: const Icon(Icons.add),
                                        tooltip: 'Add one trainee',
                                      ),
                                      IconButton(
                                        onPressed: () =>
                                            _showAddMultipleTraineeToSessionDialog(
                                                context, trainingSession),
                                        icon: const Icon(Icons.group_add),
                                        tooltip: 'Add multiple trainees',
                                      )
                                    ],
                                  )
                                ],
                              )),
                        )),
                      );
                    }))
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton.small(
          onPressed: () => _showAddTrainingSessionDialog(context),
          child: Icon(Icons.add)),
    );
  }
}
