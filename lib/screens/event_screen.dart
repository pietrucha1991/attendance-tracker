import 'package:attendance_tracker/screens/single_event_screen.dart';
import 'package:attendance_tracker/services/event/event_service.dart';
import 'package:attendance_tracker/services/eventParticipant/event_participant_service.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../models/event/eventModel/event_model.dart';
import '../models/event/eventParticipant/event_participant.dart';
import '../widgets/green_gradient.dart';

class EventScreen extends StatefulWidget {
  const EventScreen({super.key});

  @override
  State<EventScreen> createState() => _EventScreenState();
}

class _EventScreenState extends State<EventScreen> {
  List<EventParticipant> _eventParticipants = [];
  List<EventModel> _allEvents = [];
  final TextEditingController _titleEditingController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();

  final TextEditingController _participantNameController =
      TextEditingController();
  final TextEditingController _participantMoneyController =
      TextEditingController();

  DateTimeRange? _dateRange;

  Future<void> _loadAllEvents() async {
    List<EventModel> allEvents = await EventService().getAllEvents();
    setState(() {
      _allEvents = allEvents;
    });
  }

  Future<void> _pickDateRange(BuildContext context) async {
    final DateTimeRange? pickedRange = await showDateRangePicker(
        context: context,
        firstDate: DateTime(2000),
        initialDateRange: _dateRange,
        lastDate: DateTime(2100));

    if (pickedRange != null) {
      setState(() {
        _dateRange = pickedRange;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _loadAllEvents();
  }

  void _showAddEventDialog(BuildContext context) {
    showDialog(
        context: context,
        builder: (context) {
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter setStateDialog) {
            return AlertDialog(
              alignment: Alignment.center,
              title: Text('Add event'),
              content: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[
                    TextField(
                      controller: _titleEditingController,
                      decoration:
                          const InputDecoration(hintText: 'Enter title'),
                    ),
                    TextField(
                      controller: _descriptionController,
                      decoration:
                          const InputDecoration(hintText: 'Enter description'),
                    ),
                    ListTile(
                        title: const Text('Select date'),
                        onTap: () => _pickDateRange(context))
                  ],
                ),
              ),
              actions: <Widget>[
                TextButton(
                    onPressed: () => Navigator.of(context).pop(),
                    child: const Text('Cancel')),
                TextButton(
                    onPressed: () async {
                      await EventService().addEvent(
                          _titleEditingController.text,
                          _descriptionController.text,
                          _dateRange!.start,
                          _dateRange!.end);
                      _loadAllEvents();
                      Navigator.of(context).pop();
                    },
                    child: const Text('Add event'))
              ],
            );
          });
        });
  }

  void _showAddParticipantDialog(BuildContext context, EventModel eventModel) {
    _participantNameController.text = '';
    _participantMoneyController.text = '';
    bool isPaid = false;
    showDialog(
        context: context,
        builder: (context) {
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
            return AlertDialog(
              alignment: Alignment.center,
              title: Text('Add participant'),
              content: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[
                    TextField(
                      controller: _participantNameController,
                      decoration: const InputDecoration(hintText: 'Enter name'),
                    ),
                    TextField(
                      controller: _participantMoneyController,
                      decoration:
                          const InputDecoration(hintText: 'Enter money paid'),
                      keyboardType:
                          const TextInputType.numberWithOptions(decimal: true),
                    ),
                    Row(
                      children: [
                        const Text('Payment for the event'),
                        Checkbox(
                            value: isPaid,
                            onChanged: (bool? newValue) async {
                              setState(() {
                                isPaid = newValue!;
                              });
                            }),
                      ],
                    )
                  ],
                ),
              ),
              actions: [
                TextButton(
                    onPressed: () => Navigator.of(context).pop(),
                    child: const Text('Cancel')),
                TextButton(
                    onPressed: () async {
                      EventParticipant participantToAdd = EventParticipant(
                          name: _participantNameController.text,
                          moneyPaid:
                              double.parse(_participantMoneyController.text),
                          isPaid: isPaid);
                      await EventParticipantService().addParticipantToEvent(
                          eventModel.id!, participantToAdd);
                      _loadAllEvents();
                      if (context.mounted) {
                        Navigator.of(context).pop();
                      } else {
                        throw Exception('Context is not mounted.');
                      }
                    },
                    child: const Text('Add participant'))
              ],
            );
          });
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text('Events')),
        body: Container(
          child: ListView.builder(
              itemCount: _allEvents.length,
              itemBuilder: (context, index) {
                final event = _allEvents[index];

                return GreenGradient(
                  child: InkWell(
                    onTap: () {
                      Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      SingleEventScreen(eventModel: event)))
                          .then((_) => _loadAllEvents());
                    },
                    child: Card(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Stack(
                          alignment: Alignment.center,
                          children: <Widget>[
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Text(event.title,
                                    style: const TextStyle(
                                        fontWeight: FontWeight.bold)),
                                Text(
                                    '${event.description}\nGathered money ${event.amountOfMoneyPaid} PLN',
                                    textAlign: TextAlign.center),
                                Text(
                                    'Start date: ${DateFormat('dd-MM-yyyy').format(event.startDate)}'),
                                Text(
                                    'End date: ${DateFormat('dd-MM-yyyy').format(event.endDate)}'),
                              ],
                            ),
                            Align(
                              alignment: Alignment.centerRight,
                              child: IconButton(
                                icon: const Icon(Icons.group_add,
                                    size: 30), // Bigger icon
                                onPressed: () =>
                                    _showAddParticipantDialog(context, event),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              }),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: FloatingActionButton.small(
            onPressed: () => _showAddEventDialog(context),
            child: const Icon(Icons.add)));
  }
}
