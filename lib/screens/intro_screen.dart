import 'package:attendance_tracker/screens/home_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class IntroScreen extends StatefulWidget {
  @override
  _IntroScreenState createState() => _IntroScreenState();
}

class _IntroScreenState extends State<IntroScreen>
    with TickerProviderStateMixin {
  late AnimationController _logoAnimationController;
  late Animation<double> _logoAnimation;
  late Animation<double> _textAnimation;

  late AnimationController _clickTheLogoController;
  late Animation<double> _clickTheLogoAnimation;

  @override
  void initState() {
    super.initState();
    _logoAnimationController = AnimationController(
      vsync: this,
      duration: Duration(seconds: 3), // Adjust the duration as needed
    );

    _logoAnimation = Tween<double>(
            begin: 0.0, // Start position (right side)
            end: 1.8 // End position (middle)
            )
        .animate(CurvedAnimation(
      parent: _logoAnimationController,
      curve: Curves.easeIn,
    ));

    _textAnimation = Tween<double>(
      begin: 0.0,
      end: 1.0,
    ).animate(CurvedAnimation(
        parent: _logoAnimationController, curve: Curves.easeInOut));

    _clickTheLogoController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 1000));
    _clickTheLogoAnimation = Tween<double>(begin: 0.0, end: 1.3).animate(
        CurvedAnimation(
            parent: _clickTheLogoController, curve: Curves.easeInOut));

    _logoAnimationController.forward();
    _logoAnimationController.addStatusListener((status) {
      if (_logoAnimation.isCompleted) {
        _clickTheLogoController.repeat(reverse: true);
      }
    });
  }

  @override
  void dispose() {
    _logoAnimationController.dispose();
    _clickTheLogoController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Flexible(
              child: FadeTransition(
                  opacity: _textAnimation,
                  child: Text(
                    'Welcome to your personal attendance tracker',
                    textAlign: TextAlign.center,
                    style: GoogleFonts.pacifico(
                        fontSize: 24, fontWeight: FontWeight.w400),
                  )),
            ),
            Flexible(
              child: ScaleTransition(
                  scale: _logoAnimation,
                  child: InkWell(
                    onTap: () {
                      Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                              builder: (context) => HomeScreen()));
                    },
                    child: Image.asset('assets/images/file.png',
                        fit: BoxFit.fitWidth),
                  )),
            ),
            const SizedBox(height: 20),
            Flexible(
                child: FadeTransition(
                  opacity: _clickTheLogoAnimation,
                  child: Text('Click the logo to start',
                  textAlign: TextAlign.center,
                  style: GoogleFonts.pacifico(
                      fontSize: 24, fontWeight: FontWeight.w400)),
            ))
          ],
        ),
      ),
    );
  }
}
