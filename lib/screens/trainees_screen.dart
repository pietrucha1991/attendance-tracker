import 'package:attendance_tracker/screens/payment_screen.dart';
import 'package:attendance_tracker/services/payment/payment_service.dart';
import 'package:attendance_tracker/services/trainee/trainee_service.dart';
import 'package:attendance_tracker/widgets/green_gradient.dart';
import 'package:flutter/material.dart';

import '../models/payment/payment_model.dart';
import '../models/trainee/trainee.dart';
import '../services/sharedPreferences/preferences_service.dart';

class TraineesScreen extends StatefulWidget {
  const TraineesScreen({super.key});

  @override
  State<TraineesScreen> createState() => _TraineesScreenState();
}

class _TraineesScreenState extends State<TraineesScreen> {
  final TextEditingController _firstNameController = TextEditingController();
  final TextEditingController _secondNameController = TextEditingController();
  final TextEditingController _priceEditingController = TextEditingController();
  final TextEditingController _moneyPaidTextEditingController =
      TextEditingController();
  final TextEditingController _descriptionTextEditingController =
      TextEditingController();
  List<Trainee> _trainees = [];

  Future<void> _loadTrainees() async {
    List<Trainee> trainees = await TraineeService().getAllTrainees();
    setState(() {
      _trainees = trainees;
    });
  }

  Future<void> resetAllPaymentsAfterTheFirstDayOfTheMonth() async {
    DateTime today = DateTime.now();
    DateTime firstDayOfTheMonth = DateTime(today.year, today.month, 1);

    final lastResetDate = await PreferencesService().getLastResetDate();

    if (lastResetDate == null || lastResetDate.isBefore(firstDayOfTheMonth)) {
      for (var trainee in _trainees) {
        trainee.isPaid = false;
        await TraineeService().updateTrainee(trainee);
      }
      await PreferencesService().setLastResetDate(today);
    }
  }

  @override
  void initState() {
    super.initState();
    _loadTrainees();
    resetAllPaymentsAfterTheFirstDayOfTheMonth();
  }

  void _showAddTraineeDialog(BuildContext context, {Trainee? trainee}) {
    _firstNameController.text = trainee?.firstName ?? '';
    _secondNameController.text = trainee?.lastName ?? '';
    _priceEditingController.text = trainee?.trainingPrice.toString() ?? '';

    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title:
                Text(trainee == null ? 'Add a new trainee' : 'Update trainee'),
            content: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Flexible(
                    child: TextField(
                      controller: _firstNameController,
                      decoration: InputDecoration(labelText: 'First name'),
                    ),
                  ),
                  Flexible(
                    child: TextField(
                      controller: _secondNameController,
                      decoration: InputDecoration(labelText: 'Second name'),
                    ),
                  ),
                  Flexible(
                    child: TextField(
                      controller: _priceEditingController,
                      decoration: InputDecoration(labelText: 'Money paid'),
                      keyboardType:
                          TextInputType.numberWithOptions(decimal: true),
                    ),
                  )
                ],
              ),
            ),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text('Cancel')),
              TextButton(
                  onPressed: () async {
                    final price = double.parse(_priceEditingController.text);
                    if (trainee == null) {
                      final success = await TraineeService().addTrainee(
                          _firstNameController.text,
                          _secondNameController.text,
                          price);

                      Navigator.of(context).pop();
                      if (success) {
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: const Text('Trainee added successfully')));
                        _loadTrainees();
                      }
                    } else {
                      final updatedTrainee = Trainee(
                          id: trainee.id,
                          firstName: _firstNameController.text,
                          lastName: _secondNameController.text,
                          trainingPrice: price,
                          isPaid: trainee.isPaid);

                      final success =
                          await TraineeService().updateTrainee(updatedTrainee);
                      Navigator.of(context).pop();
                      if (success) {
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content:
                                const Text('Trainee edited successfully')));
                        _loadTrainees();
                      }
                    }
                  },
                  child: Text(
                      trainee == null ? 'Add a new trainee' : 'Update trainee'))
            ],
          );
        });
  }

  void _showAddPaymentDialog(BuildContext context, Trainee trainee,
      {Payment? payment}) {
    _moneyPaidTextEditingController.text = payment?.amountPaid.toString() ?? '';
    _descriptionTextEditingController.text = payment?.description ?? '';

    showDialog(
        context: context,
        builder: (context) {
          final currentDate = DateTime.now();
          late DateTime? selectedDate;
          return AlertDialog(
            title: const Text('Add payment'),
            content: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Flexible(
                    child: TextField(
                      controller: _moneyPaidTextEditingController,
                      decoration:
                          const InputDecoration(labelText: 'Money paid'),
                      keyboardType:
                          const TextInputType.numberWithOptions(decimal: true),
                    ),
                  ),
                  Flexible(
                    child: TextField(
                      controller: _descriptionTextEditingController,
                      decoration:
                          const InputDecoration(labelText: 'Description'),
                    ),
                  ),
                  ElevatedButton(
                    onPressed: () async {
                      selectedDate = await showDatePicker(
                          context: context,
                          firstDate: currentDate,
                          lastDate: currentDate.add(Duration(days: 365)));
                    },
                    child: const Text('Select date'),
                  )
                ],
              ),
            ),
            actions: [
              TextButton(
                  onPressed: () async {
                    final amountPaid =
                        double.parse(_moneyPaidTextEditingController.text);
                    bool isAdded = await PaymentService().addPayment(
                        amountPaid,
                        trainee.id!,
                        _descriptionTextEditingController.text,
                        selectedDate!);
                    Navigator.of(context).pop();
                    if (isAdded) {
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                          content: const Text('Payment added successfully')));
                    }
                  },
                  child: Text('Add payment')),
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text('Cancel'))
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Trainees list')),
      body: Column(
        children: <Widget>[
          Expanded(
            child: ListView.builder(
                itemCount: _trainees.length,
                itemBuilder: (context, index) {
                  final trainee = _trainees[index];
                  return Card(
                      elevation: 4,
                      margin: const EdgeInsets.all(8.0),
                      child: GreenGradient(
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        PaymentScreen(trainee: trainee)));
                          },
                          child: Padding(
                            padding: EdgeInsets.all(8.0),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                    child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                        '${trainee.firstName} ${trainee.lastName}'),
                                    Text(
                                        'Paid :\$${trainee.trainingPrice.toString()}'),
                                  ],
                                )),
                                Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    IconButton(
                                        icon: const Icon(Icons.delete),
                                        onPressed: () async {
                                          await TraineeService()
                                              .deleteTrainee(trainee.id!);
                                          _loadTrainees();
                                        }),
                                    IconButton(
                                        icon: const Icon(Icons.update_outlined),
                                        onPressed: () async {
                                          _showAddTraineeDialog(context,
                                              trainee: trainee);
                                        }),
                                    IconButton(
                                        onPressed: () async {
                                          _showAddPaymentDialog(
                                              context, trainee);
                                        },
                                        icon: const Icon(Icons.payment)),
                                    Checkbox(
                                        value: trainee.isPaid,
                                        onChanged: (bool? newValue) async {
                                          setState(() {
                                            trainee.isPaid = newValue!;
                                          });
                                          bool success = await TraineeService()
                                              .updateTrainee(trainee);
                                          if (success) {
                                            print(
                                                "Update successful for trainee paid status: ${trainee.isPaid}");
                                            _loadTrainees();
                                          } else {
                                            print(
                                                "Update successful for trainee paid status: ${trainee.isPaid}");
                                          }
                                        })
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ));
                }),
          ),
          Padding(
              padding: const EdgeInsets.all(20),
              child: ElevatedButton(
                onPressed: () {
                  _showAddTraineeDialog(context);
                },
                child: Text('Add trainee'),
              ))
        ],
      ),
    );
  }
}
