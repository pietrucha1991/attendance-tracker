import 'package:attendance_tracker/services/event/event_service.dart';
import 'package:attendance_tracker/widgets/event_participant_card.dart';
import 'package:attendance_tracker/widgets/green_gradient.dart';
import 'package:attendance_tracker/widgets/red_gradient.dart';
import 'package:flutter/material.dart';

import '../models/event/eventModel/event_model.dart';
import '../models/event/eventParticipant/event_participant.dart';

class SingleEventScreen extends StatefulWidget {
  final EventModel eventModel;

  const SingleEventScreen({super.key, required this.eventModel});

  @override
  State<SingleEventScreen> createState() => _SingleEventScreenState();
}

class _SingleEventScreenState extends State<SingleEventScreen>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;
  List<EventParticipant> _eventParticipants = [];

  void _loadAllParticipants() async {
    EventModel eventModel =
        await EventService().getEventById(widget.eventModel.id!);
    List<EventParticipant>? participants = eventModel.peoplePresent;
    setState(() {
      _eventParticipants = participants!;
    });
  }

  void _tabChanged() {
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, vsync: this, initialIndex: 0);
    _tabController.addListener(() {
      _tabChanged();
    });
    _loadAllParticipants();
  }

  @override
  void dispose() {
    _tabController.removeListener(_tabChanged);
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(widget.eventModel.title),
          bottom: TabBar(
            controller: _tabController,
            tabs: const [
              Tab(icon: Icon(Icons.person), text: 'Participants'),
              Tab(icon: Icon(Icons.attach_money), text: 'Expenses'),
            ],
          )),
      body: TabBarView(
        controller: _tabController,
        children: [_buildParticipantsTab(), _buildExpensesTab()],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: _tabController.index == 1
          ? FloatingActionButton(
              onPressed: () {
                _showAddExpenseDialog(context, widget.eventModel);
              },
              tooltip: 'Add expense',
              child: const Icon(Icons.add),
            )
          : null,
    );
  }

  Widget _buildParticipantsTab() {
    return ListView.builder(
        itemCount: _eventParticipants.length,
        itemBuilder: (BuildContext context, int index) {
          final participant = _eventParticipants[index];
          return EventParticipantCard(
            eventModel: widget.eventModel,
            eventParticipant: participant,
            onDeleted: () {
              _loadAllParticipants();
            },
          );
        });
  }

  Card paidCard(EventParticipant eventParticipant) {
    return Card(
      child: GreenGradient(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text('${eventParticipant.name} ${eventParticipant.moneyPaid}'),
            ],
          ),
        ),
      ),
    );
  }

  Card notPaidCard(EventParticipant eventParticipant) {
    return Card(
      child: RedGradient(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text('${eventParticipant.name} ${eventParticipant.moneyPaid}'),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildExpensesTab() {
    if (widget.eventModel.expenses == null ||
        widget.eventModel.expenses!.isEmpty) {
      return const Center(child: Text("No expenses recorded"));
    }
    return ListView.builder(
      itemCount: widget.eventModel.expenses!.length,
      itemBuilder: (BuildContext context, int index) {
        MapEntry<String, double> expense =
            widget.eventModel.expenses!.entries.elementAt(index);
        return Card(
          margin: const EdgeInsets.all(8.0),
          elevation: 4.0,
          child: ListTile(
            title: Text(
                '${expense.key} ${expense.value.toStringAsFixed(2)} PLN',
                style: TextStyle(fontSize: 18, color: Colors.green[800])),
          ),
        );
      },
    );
  }

  void _showAddExpenseDialog(BuildContext context, EventModel eventModel) {
    TextEditingController nameController = TextEditingController();
    TextEditingController amountController = TextEditingController();

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Add Expense'),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                TextField(
                  controller: nameController,
                  decoration: InputDecoration(labelText: 'Expense Name'),
                ),
                TextField(
                  controller: amountController,
                  decoration: InputDecoration(labelText: 'Amount'),
                  keyboardType: TextInputType.numberWithOptions(decimal: true),
                ),
              ],
            ),
            actions: <Widget>[
              TextButton(
                child: Text('Cancel'),
                onPressed: () => Navigator.of(context).pop(),
              ),
              TextButton(
                child: Text('Add'),
                onPressed: () async {
                  double amount = double.tryParse(amountController.text) ?? 0;
                  widget.eventModel.expenses ??= {};
                  widget.eventModel.expenses![nameController.text] = amount;
                  await EventService().updateEvent(widget.eventModel);
                  Navigator.of(context).pop();
                  // Optionally, trigger a state update if using setState or a provider
                },
              )
            ],
          );
        });
  }
}
