import 'dart:convert';

class ParseJson {
  static Map<String, double>? parseExpenses(dynamic rawExpenses) {
    if (rawExpenses == "null") {
      return null; // or return {}; if you want to ensure it always returns a map
    }

    // Check if expenses is a string and decode it if necessary
    Map<dynamic, dynamic> expensesMap = rawExpenses is String
        ? jsonDecode(rawExpenses) as Map
        : rawExpenses as Map;

    // Convert the map to Map<String, double>
    return expensesMap.map<String, double>(
        (k, v) => MapEntry(k as String, (v as num).toDouble()));
  }
}
