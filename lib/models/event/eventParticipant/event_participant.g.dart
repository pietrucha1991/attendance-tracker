// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'event_participant.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EventParticipant _$EventParticipantFromJson(Map<String, dynamic> json) =>
    EventParticipant(
      id: (json['id'] as num?)?.toInt(),
      name: json['name'] as String,
      moneyPaid: (json['moneyPaid'] as num).toDouble(),
      isPaid: json['isPaid'] == null
          ? false
          : const BoolToIntConverter().fromJson(json['isPaid'] as int),
    );

Map<String, dynamic> _$EventParticipantToJson(EventParticipant instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'moneyPaid': instance.moneyPaid,
      'isPaid': const BoolToIntConverter().toJson(instance.isPaid),
    };
