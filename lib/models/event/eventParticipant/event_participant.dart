import 'package:json_annotation/json_annotation.dart';

import '../../../utils/converter.dart';

part 'event_participant.g.dart';

@JsonSerializable()
class EventParticipant {
  int? id;
  final String name;
  final double moneyPaid;
  @BoolToIntConverter()
  bool isPaid;

  EventParticipant(
      {this.id,
      required this.name,
      required this.moneyPaid,
      this.isPaid = false});

  factory EventParticipant.fromJson(Map<String, dynamic> jsonFile) =>
      _$EventParticipantFromJson(jsonFile);

  Map<String, dynamic> toJson() => _$EventParticipantToJson(this);
}
