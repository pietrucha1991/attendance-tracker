// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'event_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EventModel _$EventModelFromJson(Map<String, dynamic> json) => EventModel(
      id: (json['id'] as num?)?.toInt(),
      title: json['title'] as String,
      description: json['description'] as String,
      peoplePresent: json['peoplePresent'] == "null"
          ? []
          : (jsonDecode(json['peoplePresent'] as String) as List)
              .map((traineeJson) => EventParticipant.fromJson(traineeJson))
              .toList(),
      amountOfMoneyPaid: (json['amountOfMoneyPaid'] as num?)!.toDouble(),
      expenses: ParseJson.parseExpenses(json['expenses']),
      startDate: DateTime.parse(json['startDate'] as String),
      endDate: DateTime.parse(json['endDate'] as String),
    );

Map<String, dynamic> _$EventModelToJson(EventModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'description': instance.description,
      'peoplePresent': jsonEncode(instance.peoplePresent
          ?.map((participant) => participant.toJson())
          .toList()),
      'amountOfMoneyPaid': instance.amountOfMoneyPaid,
      'expenses': jsonEncode(instance.expenses),
      'startDate': instance.startDate.toIso8601String(),
      'endDate': instance.endDate.toIso8601String(),
    };
