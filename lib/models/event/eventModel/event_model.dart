import 'dart:convert';

import 'package:attendance_tracker/models/event/eventParticipant/event_participant.dart';
import 'package:attendance_tracker/utils/parse_json.dart';
import 'package:json_annotation/json_annotation.dart';

part 'event_model.g.dart';

@JsonSerializable()
class EventModel {
  final int? id;
  final String title;
  final String description;
  List<EventParticipant>? peoplePresent;
  double amountOfMoneyPaid = 0.0;
  Map<String, double>? expenses;
  final DateTime startDate;
  final DateTime endDate;

  EventModel(
      {this.id,
      required this.title,
      required this.description,
      this.peoplePresent,
      this.amountOfMoneyPaid = 0.0,
      this.expenses,
      required this.startDate,
      required this.endDate});

  factory EventModel.fromJson(Map<String, dynamic> jsonFile) =>
      _$EventModelFromJson(jsonFile);

  Map<String, dynamic> toJson() => _$EventModelToJson(this);
}
