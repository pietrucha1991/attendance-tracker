// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'payment_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Payment _$PaymentFromJson(Map<String, dynamic> json) => Payment(
      id: json['id'] as int?,
      traineeId: json['traineeId'] as int,
      amountPaid: (json['amountPaid'] as num).toDouble(),
      dateOfPayment: DateTime.parse(json['dateOfPayment'] as String),
      description: json['description'] as String?,
    );

Map<String, dynamic> _$PaymentToJson(Payment instance) => <String, dynamic>{
      'id': instance.id,
      'traineeId': instance.traineeId,
      'amountPaid': instance.amountPaid,
      'dateOfPayment': instance.dateOfPayment.toIso8601String(),
      'description': instance.description,
    };
