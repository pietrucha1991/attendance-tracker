import 'package:json_annotation/json_annotation.dart';

part 'payment_model.g.dart';

@JsonSerializable()
class Payment {
  final int? id;
  final int traineeId;
  final double amountPaid;
  final DateTime dateOfPayment;
  final String? description;

  Payment(
      {this.id,
      required this.traineeId,
      required this.amountPaid,
      required this.dateOfPayment,
      this.description});

  factory Payment.fromJson(Map<String, dynamic> jsonFile) =>
      _$PaymentFromJson(jsonFile);

  Map<String, dynamic> toJson() => _$PaymentToJson(this);
}
