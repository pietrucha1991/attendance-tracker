import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';

import '../trainee/trainee.dart';
import '../training_type.dart';

part 'trainingSession.g.dart';

@JsonSerializable()
class TrainingSession {
  final int? id;
  final TrainingType trainingType;
  final DateTime? date;
  List<Trainee>? traineesPresent;

  TrainingSession(
      {this.id,
      required this.trainingType,
      required this.date,
      List<Trainee>? traineesPresent})
      : traineesPresent = traineesPresent ?? [];

  factory TrainingSession.fromJson(Map<String, dynamic> json) =>
      _$TrainingSessionFromJson(json);

  Map<String, dynamic> toJson() => _$TrainingSessionToJson(this);

  void addTrainee(Trainee trainee) {
    traineesPresent?.add(trainee);
  }
}
