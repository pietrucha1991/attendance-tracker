// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'trainingSession.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TrainingSession _$TrainingSessionFromJson(Map<String, dynamic> json) =>
    TrainingSession(
      id: json['id'] as int?,
      trainingType: $enumDecode(_$TrainingTypeEnumMap, json['trainingType']),
      date:
          json['date'] == null ? null : DateTime.parse(json['date'] as String),
      traineesPresent: (jsonDecode(json['traineesPresent'] as String) as List)
          .map((traineeJson) => Trainee.fromJson(traineeJson))
          .toList(),
    );

Map<String, dynamic> _$TrainingSessionToJson(TrainingSession instance) =>
    <String, dynamic>{
      'id': instance.id,
      'trainingType': _$TrainingTypeEnumMap[instance.trainingType]!,
      'date': instance.date?.toIso8601String(),
      'traineesPresent': jsonEncode(instance.traineesPresent
          ?.map((trainee) => trainee.toJson())
          .toList()),
    };

const _$TrainingTypeEnumMap = {
  TrainingType.CHILDREN: 'CHILDREN',
  TrainingType.ADULTS: 'ADULTS',
};
