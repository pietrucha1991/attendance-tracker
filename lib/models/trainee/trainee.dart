import 'package:attendance_tracker/utils/converter.dart';
import 'package:json_annotation/json_annotation.dart';

part 'trainee.g.dart';

@JsonSerializable()
class Trainee {
  final int? id;
  final String firstName;
  final String lastName;
  final double trainingPrice;
  @BoolToIntConverter()
  bool isPaid;

  Trainee(
      {this.id,
      required this.firstName,
      required this.lastName,
      required this.trainingPrice,
      this.isPaid = false});

  factory Trainee.fromJson(Map<String, dynamic> json) =>
      _$TraineeFromJson(json);

  Map<String, dynamic> toJson() => _$TraineeToJson(this);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Trainee &&
        other.id == id &&
        other.firstName == firstName &&
        other.lastName == lastName;
  }

  @override
  int get hashCode => id.hashCode ^ firstName.hashCode ^ lastName.hashCode;
}
