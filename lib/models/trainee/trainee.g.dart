// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'trainee.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Trainee _$TraineeFromJson(Map<String, dynamic> json) => Trainee(
      id: json['id'] as int?,
      firstName: json['firstName'] as String,
      lastName: json['lastName'] as String,
      trainingPrice: (json['trainingPrice'] as num).toDouble(),
      isPaid: json['isPaid'] == null
          ? false
          : const BoolToIntConverter().fromJson(json['isPaid'] as int),
    );

Map<String, dynamic> _$TraineeToJson(Trainee instance) => <String, dynamic>{
      'id': instance.id,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'trainingPrice': instance.trainingPrice,
      'isPaid': const BoolToIntConverter().toJson(instance.isPaid),
    };
