import 'package:attendance_tracker/screens/intro_screen.dart';
import 'package:flutter/material.dart';

import 'services/db/database_helper.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool _isDbInitialized = false;

  @override
  void initState() {
    super.initState();
    initializeDatabase();
    print('DB initialized');
  }

  void initializeDatabase() async {
    await DatabaseHelper.instance.database;
    setState(() {
      _isDbInitialized = true; // Update the state to reflect DB is initialized
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: themeData(context),
      home: IntroScreen(),
    );
  }

  ThemeData themeData(BuildContext context) {
    var greenColor = Colors.green[900] ?? Colors.green;
    return ThemeData(
        tabBarTheme: TabBarTheme(
            indicatorSize: TabBarIndicatorSize.tab,
            labelColor: Colors.green,
            unselectedLabelColor: Colors.green[900],
            indicator: BoxDecoration(
                border:
                    Border(bottom: BorderSide(color: greenColor, width: 2.0))),
            labelStyle:
                const TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
            unselectedLabelStyle: const TextStyle(fontSize: 14.0)),
        appBarTheme: const AppBarTheme(
            color: Colors.transparent,
            iconTheme: IconThemeData(color: Colors.white)),
        colorScheme: ColorScheme.fromSeed(
                seedColor: Colors.green[800] ?? Colors.green,
                brightness: Brightness.dark)
            .copyWith(primary: Colors.white),
        useMaterial3: true,
        primaryColor: greenColor,
        cardColor: Colors.blue,
        textTheme: const TextTheme(
          displayLarge: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
          bodyLarge: TextStyle(fontSize: 12, fontWeight: FontWeight.bold),
        ),
        elevatedButtonTheme: ElevatedButtonThemeData(
            style: ButtonStyle(
                padding: MaterialStateProperty.all(EdgeInsets.all(5.0)),
                shape: MaterialStatePropertyAll<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                )),
                backgroundColor: MaterialStateProperty.resolveWith((states) {
                  return Colors.transparent;
                }),
                foregroundColor: MaterialStateProperty.all(Colors.white))));
  }
}
